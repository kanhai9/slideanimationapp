package com.myanimationbanner;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.adapter.PageViewAdapter;

import java.util.Timer;
import java.util.TimerTask;

public class Main6Activity extends AppCompatActivity {

    int[] flag;
    private ViewPager mBannerViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allpager_view);

        flag = new int[] { R.drawable.wall_one, R.drawable.wall_two,
                R.drawable.wall_three, R.drawable.wall_four ,R.drawable.wall_one, R.drawable.wall_two,
                R.drawable.wall_three, R.drawable.wall_four};

        mBannerViewPager =(ViewPager) findViewById(R.id.bannerViewPager);

        // Pass results to ViewPagerAdapter Class
        PageViewAdapter adapter = new PageViewAdapter(Main6Activity.this, flag);
        // Binds the Adapter to the ViewPager
        mBannerViewPager.setAdapter(adapter);
        mBannerViewPager.setClipToPadding(false);
        AutoSwipeBanner();
        mBannerViewPager.setPageTransformer(true, new ZoomOutPageTransformer());



    }




    public class ZoomOutPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.85f;
        private static final float MIN_ALPHA = 0.5f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            int pageHeight = view.getHeight();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 1) { // [-1,1]
                // Modify the default slide transition to shrink the page as well
                float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                if (position < 0) {
                    view.setTranslationX(horzMargin - vertMargin / 2);
                } else {
                    view.setTranslationX(-horzMargin + vertMargin / 2);
                }

                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

                // Fade the page relative to its size.
                view.setAlpha(MIN_ALPHA +
                        (scaleFactor - MIN_SCALE) /
                                (1 - MIN_SCALE) * (1 - MIN_ALPHA));

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }

    }





    public void AutoSwipeBanner(){
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                int currentPage=mBannerViewPager.getCurrentItem();
                if (currentPage == flag.length-1) {
                    currentPage = -1;
                }
                mBannerViewPager.setCurrentItem(currentPage+1, true);
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 300, 4000);

    }

}
