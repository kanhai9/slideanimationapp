package com.myanimationbanner;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.adapter.PageViewAdapter;

import java.util.Timer;
import java.util.TimerTask;

public class Main2Activity extends AppCompatActivity {


    int[] flag;
    private ViewPager mBannerViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allpager_view);

        flag = new int[] { R.drawable.wall_one, R.drawable.wall_two,
                R.drawable.wall_three, R.drawable.wall_four ,R.drawable.wall_one, R.drawable.wall_two,
                R.drawable.wall_three, R.drawable.wall_four};

        mBannerViewPager =(ViewPager) findViewById(R.id.bannerViewPager);

        // Pass results to ViewPagerAdapter Class
        PageViewAdapter adapter = new PageViewAdapter(Main2Activity.this, flag);
        // Binds the Adapter to the ViewPager
        mBannerViewPager.setAdapter(adapter);
        mBannerViewPager.setClipToPadding(false);
        // set padding manually, the more you set the padding the more you see of prev & next page
        mBannerViewPager.setPadding(60, 0, 60, 0);


        AutoSwipeBanner();

    }





    public void AutoSwipeBanner(){
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                int currentPage=mBannerViewPager.getCurrentItem();
                if (currentPage == flag.length-1) {
                    currentPage = -1;
                }
                mBannerViewPager.setCurrentItem(currentPage+1, true);
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 100, 4000);

    }

}
