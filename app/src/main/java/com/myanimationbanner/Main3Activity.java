package com.myanimationbanner;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.adapter.PageViewAdapter;

import java.util.Timer;
import java.util.TimerTask;

public class Main3Activity extends AppCompatActivity {

    int[] flag;
    private ViewPager mBannerViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allpager_view);

        flag = new int[] { R.drawable.wall_one, R.drawable.wall_two,
                R.drawable.wall_three, R.drawable.wall_four ,R.drawable.wall_one, R.drawable.wall_two,
                R.drawable.wall_three, R.drawable.wall_four};

        mBannerViewPager =(ViewPager) findViewById(R.id.bannerViewPager);

        // Pass results to ViewPagerAdapter Class
        PageViewAdapter adapter = new PageViewAdapter(Main3Activity.this, flag);
        // Binds the Adapter to the ViewPager
        mBannerViewPager.setAdapter(adapter);
        mBannerViewPager.setClipToPadding(false);
        // set padding manually, the more you set the padding the more you see of prev & next page
        mBannerViewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                /**SCALING TRANSFORMATION**/
                final float normalizedposition = Math.abs(Math.abs(position) - 1);
                page.setScaleX(normalizedposition / 2 + 0.5f);
                page.setScaleY(normalizedposition / 2 + 0.5f);

            }
        });

        AutoSwipeBanner();

    }





    public void AutoSwipeBanner(){
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                int currentPage=mBannerViewPager.getCurrentItem();
                if (currentPage == flag.length-1) {
                    currentPage = -1;
                }
                mBannerViewPager.setCurrentItem(currentPage+1, true);
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(Update);
            }
        }, 100, 4000);

    }

}
