package com.myanimationbanner;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;

/**
 * Created by krishan on 30/5/17.
 */

public class Main7Activity extends Activity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{
    private SliderLayout mSliderLayout;
    private ArrayList<String> mStringList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slideviews);

        mSliderLayout = (SliderLayout) findViewById(R.id.pagerSlide);

        mStringList = new ArrayList<>();
        mStringList.add("http://androidblog.esy.es/images/cupcake-1.png");
        mStringList.add("http://androidblog.esy.es/images/donut-2.png");
        mStringList.add("http://androidblog.esy.es/images/eclair-3.png");
        mStringList.add("http://androidblog.esy.es/images/froyo-4.png");
        mStringList.add("http://androidblog.esy.es/images/gingerbread-5.png");

        for (String ss : mStringList) {

          /*  TextSliderView textSliderView = new TextSliderView(Main7Activity.this);

            textSliderView.image(ss)
                    .setOnSliderClickListener(this);

            mSliderLayout.addSlider(textSliderView);*/

            DefaultSliderView defaultSliderView = new DefaultSliderView(this);
            defaultSliderView.image(ss); // adding images here
            mSliderLayout.addSlider(defaultSliderView);

        }

        mSliderLayout.setCustomAnimation(new DescriptionAnimation());
        mSliderLayout.addOnPageChangeListener(this);
        mSliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);

        findViewById(R.id.bt1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);
                mSliderLayout.setDuration(3000);

            }
        });

        findViewById(R.id.bt2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Background2Foreground);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.CubeIn);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.DepthPage);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Fade);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipPage);
                mSliderLayout.setDuration(3000);
            }
        });

        findViewById(R.id.bt9).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Foreground2Background);
                mSliderLayout.setDuration(3000);

            }
        });

        findViewById(R.id.bt10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.RotateDown);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt11).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.RotateUp);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt12).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Stack);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt13).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Tablet);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt14).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.ZoomIn);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.ZoomOutSlide);
                mSliderLayout.setDuration(3000);
            }
        });
        findViewById(R.id.bt16).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
                mSliderLayout.setDuration(3000);
            }
        });


    }

    @Override
    protected void onStop() {
        super.onStop();
        mSliderLayout.stopAutoCycle();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
